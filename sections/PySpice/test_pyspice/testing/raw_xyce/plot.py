# !/usr/bin/env python3
import re
import pandas as pd
import matplotlib.pyplot as plt

filename = "./source_raw.cir.prn"

comp_spaces = re.compile(r' +')
comp_ends = re.compile(r' *\n')
new_lines = []
with open(filename, 'r') as file:
    for line in file:
        new_lines.append(
            comp_ends.sub('', comp_spaces.sub(' ', line)).split(' '))

data = pd.DataFrame(new_lines[1:-1], columns=new_lines[0], dtype=float)

print(len(data['V(L)']))
plt.plot(data['TIME'], data['V(L)'])
plt.show()
plt.savefig('./source_raw.png')

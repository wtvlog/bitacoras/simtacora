# r# This example shows a capacitive power supply with a post zener half-rectification, a kind
# r# of transformless power supply.

# r# To go further on this topic, you can read these design notes:
#r#
# r# * Transformerless Power Supply Design, Designer Circuits, LLC
# r# * Low-cost power supply for home appliances, STM, AN1476
# r# * Transformerless Power Supplies: Resistive and Capacitive, Microchip, AN954

####################################################################################################

from PySpice.Unit import *
from PySpice.Spice.Netlist import Circuit
from PySpice.Spice.Library import SpiceLibrary
from PySpice.Probe.Plot import plot
from PySpice.Doc.ExampleTools import find_libraries
import matplotlib.pyplot as plt
import numpy as np

####################################################################################################

import PySpice.Logging.Logging as Logging
logger = Logging.setup_logging()
logger.setLevel(logging.INFO)


simulators_name = ['ngspice-subprocess', 'xyce-serial']
results = []
for simulator_name in simulators_name:
    circuit = Circuit('Capacitive Half-Wave Rectification (Post Zener)')

    ac_line = circuit.AcLine('input', 'L', circuit.gnd, rms_voltage=230@u_V, frequency=50@u_Hz)
    simulator = circuit.simulator(
        simulator=simulator_name,
        xyce_command='/home/acuerol/XyceInstall/Serial/bin/Xyce',
        temperature=25, nominal_temperature=25)

    step_time = 1e-6
    end_time = 0.2
    analysis = simulator.transient(step_time=step_time, end_time=end_time)
    results.append(analysis['L'])

    figure, ax = plt.subplots(figsize=(20, 10))
    ax.plot(np.linspace(0, end_time, len(analysis['L'])), analysis['L'])
    ax.legend(('Vin [V]', 'Vout [V]'), loc=(.8, .8))
    ax.grid()
    ax.set_xlabel('t [s]')
    ax.set_ylabel('[V]')
    ax.set_xlim(0, end_time)

    plt.savefig(f'{simulator_name}_source.png')

print(f'Should be {end_time / step_time} points.')

figure, ax = plt.subplots(figsize=(20, 10))
for result in results:
    print(len(result))
    ax.plot(np.linspace(0, end_time, len(result)), result)

ax.legend(simulators_name, loc=(.8, .8))
ax.grid()
ax.set_xlabel('t [s]')
ax.set_ylabel('[V]')
ax.set_xlim(0, end_time)

plt.savefig('comparison_source.png')
